#include "main.h"
#include "stm32f1xx_hal.h"
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

extern UART_HandleTypeDef huart2;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart1_tx;

#define TOUT 1000
#define OUTPUT_SIZE 255
#define LF "\r\n"
#define BUF3 "___"

void usartTransmit(uint8_t output[]);
void IT_Usart();
void btnReadAndBlink();
