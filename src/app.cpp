
#include "app.h"

uint8_t str[OUTPUT_SIZE] = {0};
uint8_t dma[] = "USART DMA";
char input[21] = {0};
uint8_t buffer[] = BUF3;

/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
  if (htim->Instance == TIM2) {
    IT_Usart();
  }
  if (htim->Instance == TIM3) {
    HAL_GPIO_TogglePin(OUT_GPIO_Port, OUT_Pin);
  }
  if (htim->Instance == TIM4) {
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
  }
}

void IT_Usart() {
  uint8_t str[] = "USART Hello World!";
  usartTransmit(str);
}

void usartTransmit(uint8_t output[]) {
  // strcat (output, LF);

  HAL_UART_Transmit(&huart2, (uint8_t*)output, strlen((char*)output) - 1, TOUT);
  HAL_UART_Transmit(&huart2, (uint8_t*)LF, strlen((char*)LF), TOUT);
  HAL_UART_Transmit(&huart2, (uint8_t*)buffer, strlen((char*)buffer), TOUT);
  HAL_UART_Transmit(&huart2, (uint8_t*)LF, strlen((char*)LF), TOUT);
  // HAL_UART_Transmit_DMA(&huart2, dma, sizeof(dma) -1);
  /*
  if (hdma_usart1_rx.State == 0x11) {
    input[20] = 0;
    HAL_UART_Transmit_DMA(&huart2, input, sizeof(input) -1);
    HAL_UART_Receive_DMA(&huart2, (uint8_t*) input, 20);
    hdma_usart1_rx.State = HAL_DMA_STATE_BUSY;
  }*/
}
uint8_t btn_str[] = "BTN";
void btnReadAndBlink() {
  int btn = HAL_GPIO_ReadPin(TOUCH_GPIO_Port, TOUCH_Pin);
  if (btn == GPIO_PIN_SET) {
    HAL_GPIO_WritePin(OUT_GPIO_Port, OUT_Pin, GPIO_PIN_SET);
    strcpy((char*)buffer, (char*)btn_str);
    // usartTransmit(btn_str);
  }
  else {
    int btn2 = HAL_GPIO_ReadPin(TOUCH2_GPIO_Port, TOUCH2_Pin);
    int btn3 = HAL_GPIO_ReadPin(TOUCH3_GPIO_Port, TOUCH3_Pin);
    if (btn2 == GPIO_PIN_SET) {
      strcpy((char*)buffer, (char*)"xxx2");
    }
    else if (btn3 == GPIO_PIN_SET) {
      strcpy((char*)buffer, (char*)"yyy3");
    }
    else {
      strcpy((char*)buffer, (char*)BUF3);
      HAL_GPIO_WritePin(OUT_GPIO_Port, OUT_Pin, GPIO_PIN_RESET);
    }
  }
}
